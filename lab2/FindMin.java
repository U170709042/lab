public class FindMin {

    public static void main(String[] args) {
        int value1 = Integer.parseInt(args[0]);
        int value2 = Integer.parseInt(args[1]);
        int value3 = Integer.parseInt(args[2]);

        int temp = value1 < value2 ? value1 : value2;
        int result = temp < value3 ? temp : value3;

        System.out.println(result);
    }
}